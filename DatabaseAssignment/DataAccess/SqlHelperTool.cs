﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;


namespace DatabaseAssignment.DataAccess
{
    class SqlHelperTool
    {
        public SqlConnectionStringBuilder Builder { get; set; }
        public SqlHelperTool(SqlConnectionStringBuilder builder)
        {
            Builder = builder;
        }

        public SqlConnection GetConnection()
        {
            return new SqlConnection(Builder.ConnectionString);
        }

        public SqlCommand GetSqlCommand(string sql)
        {
            return new SqlCommand(sql, GetConnection());
        }


        public string NullableParser(SqlDataReader reader, string key)
        {
            return !(reader[key] is DBNull) ? (string)reader[key] : string.Empty;
        }
    }
}
