using System.Collections.Generic;
using DatabaseAssignment.Models;

namespace DatabaseAssignment.DataAccess
{
    interface ICustomerRepository
    {
        /// <summary>
        /// Interface for the CustomerRepository which consists of 9 methods for interacting with Customers in the Db
        /// </summary>
        interface ICustomerRepository
        {
            /// <summary>
            /// Method for getting customers from Db
            /// </summary>
            /// <returns>IEnumarable<Customer></returns>
            IEnumerable<Customer> GetCustomers();

            /// <summary>
            /// Method for getting multiple customers
            /// </summary>
            /// <param name="offset">Type int. Describes how many rows to skip before starting to returning rows.</param>
            /// <param name="limit">Type int. Describes how many rows to return. </param>
            /// <returns>A collection of IENumerable<Customer></returns>
            IEnumerable<Customer> GetCustomers(int offset, int limit);

            /// <summary>
            /// Method for getting a specific Customer based on Id
            /// </summary>
            /// <param name="id">type int, unique identifier of customer</param>
            /// <returns>A specific Customer</returns>
            Customer GetCustomerById(int id);

            /// <summary>
            /// Fetches the first match of a customer with partial matches on firstname and lastname.
            /// </summary>
            /// <param name="firstName">Type string. The first parameter in the SQL query</param>
            /// <param name="lastName">Type string. The second parameter in the SQL query</param>
            /// <returns>A specific Customer</returns>
            Customer GetCustomerByName(string firstName, string lastName);

            /// <summary>
            /// Add customer to database
            /// </summary>
            /// <param name="customer">Type Customer. Describes a customer to add to the Db.</param>
            /// <returns>Boolean value, true if number of rows affected larger than or equal to 1, false if nonsuccessful or no changes made</returns>
            bool AddCustomer(Customer customer);

            /// <summary>
            /// Updates an existing customer based on CustomerId.
            /// </summary>
            /// <param name="customer">A Customer object. Describes which customer to update</param>
            /// <returns>Boolean value, true if number of rows changed larger than or equal to 1, false if exception thrown or no rows changed.</returns>
            bool UpdateCustomer(Customer customer);

            /// <summary>
            /// Gets descending sorted number of customers in each country.
            /// </summary>
            /// <returns>A collection of IEnumerable<CustomerCountry> in descending order</returns>
            IEnumerable<CustomerCountry> GetCustomersByCountryDescending();

            /// <summary>
            /// Fetches the the total amount of money spent by each customer in descending order.
            /// </summary>
            /// <returns>A sorted collection of IEnumerable<CustomerSpender></returns>
            IEnumerable<CustomerSpender> GetHighestSpendersDescending();

            /// <summary>
            /// Fetches the most popular genre(s) for a given customer. 
            /// Matches on ID and throws error if not provided.
            /// </summary>
            /// <param name="customer">Type Customer. Describes the customer to find most popular genres for</param>
            /// <returns>CustomerGenre</returns>
            CustomerGenre GetMostPopularGenre(Customer customer);
        }
    }
}

