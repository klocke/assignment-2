using DatabaseAssignment.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace DatabaseAssignment.DataAccess
{

    /// <summary>
    /// Repository for queries and mappings between database and business logic. 
    /// </summary>
    class CustomerRepository : ICustomerRepository
    {
        /// <summary>
        /// Instance of SqlHelperTool: Class that holds the connectionstring and help methods
        /// </summary>
        private SqlHelperTool SqlTool { get; init; }

        /// <summary>
        ///  Constructor: Uses a connectionstringbuilder to instanciate connectionHelper
        /// </summary>
        /// <param name="builder">A SqlConnectionStringBuilder from the SqlClient package</param>
        public CustomerRepository(SqlConnectionStringBuilder builder)
        {
            SqlTool = new(builder);
        }



        /// <summary>
        /// Return single customer by specific CustomerId.
        /// Throws argumentexception if id ==0
        /// </summary>
        /// <param name="id">Type int. Uniquely describes which Customer to return</param>
        /// <returns>Customer</returns>
        /// <exception cref="ArgumentException">Thrown if argument is not valid</exception>
        public Customer GetCustomerById(int id)
        {
            if (id == 0)
            {
                throw new ArgumentException("customer Id must be specified");
            }
            string sql = "SELECT * FROM Customer WHERE CustomerId=@CustomerId";


            Customer customer;
            try
            {

                using (SqlCommand command = SqlTool.GetSqlCommand(sql))
                {

                    command.Parameters.AddWithValue("@CustomerId", id);
                    command.Connection.Open();
                    using SqlDataReader reader = command.ExecuteReader();
                    reader.Read();
                    customer = new Customer
                    {
                        Id = (int)reader["CustomerId"],
                        FirstName = (string)reader["FirstName"],
                        LastName = (string)reader["LastName"],
                        Email = (string)reader["Email"],
                        Country = SqlTool.NullableParser(reader, "Country"),
                        PhoneNumber = SqlTool.NullableParser(reader, "Phone"),
                        PostalCode = SqlTool.NullableParser(reader, "PostalCode")
                    };
                }
                return customer;


            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
            catch (InvalidOperationException ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }



            
        }

        /// <summary>
        /// Fetches the first match of a customer with partial matches on firstname and lastname.
        /// </summary>
        /// <param name="firstName">Type string. The first parameter in the SQL query</param>
        /// <param name="lastName">Type string. The second parameter in the SQL query</param>
        /// <returns>A specific Customer</returns>
        public Customer GetCustomerByName(string firstName, string lastName)
        {
            string sql = "SELECT * FROM Customer WHERE (FirstName LIKE '%@FirstName%') AND (LastName LIKE '%@LastName%')";
            Customer customer;
            try
            {

                using (SqlCommand command = SqlTool.GetSqlCommand(sql))
                {
                    command.Parameters.AddWithValue("@FirstName", firstName);
                    command.Parameters.AddWithValue("@LastName", lastName);


                    command.Connection.Open();
                    using SqlDataReader reader = command.ExecuteReader();
                    reader.Read();
                    customer = new Customer
                    {
                        Id = (int)reader["CustomerId"],
                        FirstName = (string)reader["FirstName"],
                        LastName = (string)reader["LastName"],
                        Email = (string)reader["Email"],
                        Country = SqlTool.NullableParser(reader, "Country"),
                        PhoneNumber = SqlTool.NullableParser(reader, "Phone"),
                        PostalCode = SqlTool.NullableParser(reader, "PostalCode")
                    };
                }
                return customer;


            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
            catch (InvalidOperationException ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }


        /// <summary>
        /// Gets all customers. Prints to console and returns IEnumerable<Customer> with some fields filled out.
        /// </summary>
        /// <returns>A collection of IEnumerable<Customer></returns>
        public IEnumerable<Customer> GetCustomers()
        {
            string sql = "SELECT * FROM Customer";
            List<Customer> customers = new();
            try
            {

                using (SqlCommand command = SqlTool.GetSqlCommand(sql))
                {
                    command.Connection.Open();
                    using SqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {

                        Customer customer = new()
                        {
                            Id = (int)reader["CustomerId"],
                            FirstName = (string)reader["FirstName"],
                            LastName = (string)reader["LastName"],
                            Email = (string)reader["Email"],
                            Country = SqlTool.NullableParser(reader, "Country"),
                            PhoneNumber = SqlTool.NullableParser(reader, "Phone"),
                            PostalCode = SqlTool.NullableParser(reader, "PostalCode")
                        };
                        customers.Add(customer);
                        Console.WriteLine(customer);
                    }
                }

                return customers;
            }

            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }

        }

        /// <summary>
        /// Get multiple customers based on offset and limit.
        /// </summary>
        /// <param name="offset">Type int. Describes how many rows to skip before starting to return rows.</param>
        /// <param name="limit">Type int. Describes how many rows to return.</param>
        /// <returns>A collection of IENumerable<Customer></returns>
        public IEnumerable<Customer> GetCustomers(int offset, int limit)
        {
            string sql = "SELECT * FROM Customer ORDER BY CustomerId OFFSET @Offset ROWS FETCH NEXT @Limit ROWS ONLY";
            List<Customer> customers = new();
            try
            {

                using (SqlCommand command = SqlTool.GetSqlCommand(sql))
                {


                    command.Parameters.AddWithValue("@Offset", offset);
                    command.Parameters.AddWithValue("@Limit", limit);
                    command.Connection.Open();
                    using SqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {

                        Customer customer = new()
                        {
                            Id = (int)reader["CustomerId"],
                            FirstName = (string)reader["FirstName"],
                            LastName = (string)reader["LastName"],
                            Email = (string)reader["Email"],
                            Country = SqlTool.NullableParser(reader, "Country"),
                            PhoneNumber = SqlTool.NullableParser(reader, "Phone"),
                            PostalCode = SqlTool.NullableParser(reader, "PostalCode")
                        };
                        customers.Add(customer);
                        Console.WriteLine(customer);
                    }
                }

                return customers;
            }

            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Add customer to database
        /// </summary>
        /// <param name="customer">Type Customer. Describes a customer to add to the Database.</param>
        /// <returns>Boolean value, true if number of rows affected larger than or equal to 1, false if nonsuccessful or no changes made</returns>
        /// <exception cref="SqlException">Thrown when SQL Server returns a warning or error</exception>
        public bool AddCustomer(Customer customer)
        {
            string sql;
            if (customer.Id != 0) // If customer is created with specified Id, the database should reflect this. 
            {
                Console.WriteLine("WARNING: IDENTITY_INSERT temporarily turned off!");
                sql = "INSERT INTO Customer " +
                "(CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email) " +
                "VALUES (@Id, @FirstName, @LastName, @Country, @PostalCode, @Phone, @Email)";
                sql = "SET IDENTITY_INSERT Customer ON " + sql + " SET IDENTITY_INSERT Customer OFF";
            }
            else
                sql = "INSERT INTO Customer " +
                "(FirstName, LastName, Country, PostalCode, Phone, Email) " +
                "VALUES (@FirstName, @LastName, @Country, @PostalCode, @Phone, @Email)";


            try
            {

                using SqlCommand command = SqlTool.GetSqlCommand(sql);
                command.Connection.Open();
                if (customer.Id != 0)
                {
                    command.Parameters.AddWithValue("@Id", customer.Id);
                }

                command.Parameters.AddWithValue("@FirstName", customer.FirstName);
                command.Parameters.AddWithValue("@LastName", customer.LastName);
                command.Parameters.AddWithValue("@Country", customer.Country);
                command.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                command.Parameters.AddWithValue("@Phone", customer.PhoneNumber);
                command.Parameters.AddWithValue("@Email", customer.Email);

                return command.ExecuteNonQuery() >= 1;
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }


        /// <summary>
        /// Returns a sorted list of all countries with customers. Sorts descending in number of customers. 
        /// </summary>
        /// <returns>A collection of IEnumerable<CustomerCountry> in descending order</returns>
        /// <exception cref="SqlException">Thrown when SQL Server returns warning or error</exception>
        public IEnumerable<CustomerCountry> GetCustomersByCountryDescending()
        {
            string customerCount = "counter";
            string sql = $"SELECT Country, COUNT(*) AS {customerCount} FROM Customer GROUP BY Country ORDER BY {customerCount} DESC";

            List<CustomerCountry> customerCountries = new();
            try
            {
                using (SqlCommand command = SqlTool.GetSqlCommand(sql))
                {
                    command.Connection.Open();
                    using SqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        CustomerCountry customerCountry = new()
                        {
                            CustomerCount = (int)reader[customerCount],
                            Country = (string)reader["Country"]
                        };
                        customerCountries.Add(customerCountry);
                        Console.WriteLine(customerCountry);
                    }
                }
                return customerCountries;
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }

        }

        /// <summary>
        /// Fetches the the total amount of money spent by each customer in descending order.
        /// </summary>
        /// <returns>A sorted collection of IEnumerable<CustomerSpender></returns>
        /// <exception cref="SqlException">Thrown when SQL Server returns warning or error</exception>
        public IEnumerable<CustomerSpender> GetHighestSpendersDescending()
        {
            string invoiceColumnName = "sumtotal";
            string sql = $"SELECT C.*, I.{invoiceColumnName} " +
                $"FROM Customer C " +
                $"INNER JOIN ( SELECT CustomerId, SUM(Total) as {invoiceColumnName} " +
                $"FROM Invoice GROUP BY CustomerId ) I " +
                $"ON C.CustomerId = I.CustomerId " +
                $"ORDER BY {invoiceColumnName} DESC;";

            List<CustomerSpender> customerSpenders = new();
            try
            {
                using (SqlCommand command = SqlTool.GetSqlCommand(sql))
                {
                    command.Connection.Open();
                    using SqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        CustomerSpender customerSpender = new()
                        {

                            HighSpender = new()
                            {
                                Id = (int)reader["CustomerId"],
                                FirstName = (string)reader["FirstName"],
                                LastName = (string)reader["LastName"],
                                Email = (string)reader["Email"],
                                Country = SqlTool.NullableParser(reader, "Country"),
                                PhoneNumber = SqlTool.NullableParser(reader, "Phone"),
                                PostalCode = SqlTool.NullableParser(reader, "PostalCode")
                            },
                            CustomerInvoice = (decimal)reader[invoiceColumnName]



                        };
                        customerSpenders.Add(customerSpender);
                        Console.WriteLine(customerSpender);
                    }
                }
                return customerSpenders;
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }

        }

        /// <summary>
        /// Fetches the most popular genre(s) for a given customer. 
        /// Matches on ID and throws error if not provided.
        /// </summary>
        /// <param name="customer">Type Customer. Describes the customer to find most popular genres for</param>
        /// <returns>CustomerGenre</returns>
        /// <exception cref="SqlException">Thrown when SQL Server returns warning or error</exception>
        public CustomerGenre GetMostPopularGenre(Customer customer)
        {
            if (customer.Id == 0)
            {
                throw new ArgumentException("customer Id must be specified");
            }


            string sql = "select * from Customer C join (select Cust.CustomerId, G.Name, Count(G.Name) AS val " +
                "FROM 	Customer Cust " +
                "INNER JOIN Invoice I ON I.CustomerId = Cust.CustomerId " +
                "INNER JOIN InvoiceLine Invol ON Invol.InvoiceId = I.InvoiceId " +
                "INNER JOIN Track Tr ON Tr.TrackId = Invol.TrackId " +
                "INNER JOIN Genre G on Tr.GenreId = G.GenreId " +
                $"WHERE Cust.CustomerId=  @CustomerId " +
                "GROUP BY G.Name, Cust.CustomerId ) K on C.CustomerId = K.CustomerId " +
                "ORDER by val DESC";


            CustomerGenre customerGenre = new() { GenreCustomer = customer, };
            int maxGenreCount = 0;

            List<string> mostPopularGenre = new();
            try
            {
                using (SqlCommand command = SqlTool.GetSqlCommand(sql))
                {

                    command.Parameters.AddWithValue("@CustomerId", customer.Id);
                    command.Connection.Open();
                    using SqlDataReader reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        if ((int)reader["val"] >= maxGenreCount)
                        {
                            maxGenreCount = (int)reader["val"];
                            mostPopularGenre.Add((string)reader["Name"]);
                        }
                    }
                }
                customerGenre.MostPopularGenre = mostPopularGenre;
                return customerGenre;
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }



        /// <summary>
        /// Updates an existing customer based on CustomerId.
        /// </summary>
        /// <param name="customer"></param>
        /// <returns></returns>
        public bool UpdateCustomer(Customer customer)
        {
            string sql = "UPDATE Customer SET " +
                "FirstName=@FirstName, " +
                "LastName=@LastName, " +
                "Country=@Country, " +
                "PostalCode=@PostalCode, " +
                "Phone=@Phone, " +
                "Email=@Email " +
                "WHERE CustomerId=@CustomerId";

            try
            {

                using SqlCommand command = SqlTool.GetSqlCommand(sql);
                command.Connection.Open();

                command.Parameters.AddWithValue("@FirstName", customer.FirstName);
                command.Parameters.AddWithValue("@LastName", customer.LastName);
                command.Parameters.AddWithValue("@Country", customer.Country);
                command.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                command.Parameters.AddWithValue("@Phone", customer.PhoneNumber);
                command.Parameters.AddWithValue("@Email", customer.Email);
                command.Parameters.AddWithValue("@CustomerId", customer.Id);
                return command.ExecuteNonQuery() >= 1;
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }

        }

    }
}
