using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseAssignment.Models
{
    class CustomerSpender
    {
        /// <summary>
        /// Customer whose invoice total is returned
        /// </summary>
       public Customer HighSpender { get; set; } 

        /// <summary>
        /// Total of all invoices for given customer. 
        /// </summary>
       public decimal CustomerInvoice { get; set; }


        /// <summary>
        /// Override for string representation
        /// </summary>
        /// <returns>string</returns>
        public override string ToString()
        {
            return $"{HighSpender.FirstName} {HighSpender.LastName}".PadLeft(24) + $":\t{CustomerInvoice}";
        }

    }
}
