using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseAssignment.Models
{

    /// <summary>
    /// Main class relevant for business logic related to customers. 
    /// </summary>
    public class Customer
    {
        // Important properties / nullables :

        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }

        //nullable properties:

        public string PhoneNumber { get; set; }
        public string Country { get; set; }
        public string PostalCode { get; set; }


        /// <summary>
        /// String representation of customer
        /// </summary>
        /// <returns>string</returns>
        public override string ToString()
        {
            return $"Id: {Id}\t Name: {FirstName} {LastName}\t Country: {Country}\t Postal Code: {PostalCode}\t Phone Number: {PhoneNumber}\t Email: {Email}";
        }
    }
}
