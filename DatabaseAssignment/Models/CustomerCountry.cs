using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseAssignment.Models
{
    /// <summary>
    /// Class for mapping query of customercountry.
    /// </summary>
    class CustomerCountry
    {
        /// <summary>
        /// What country is returned
        /// </summary>
        public string Country { get; set; }

        /// <summary>
        /// How many customers in each country
        /// </summary>
        public int CustomerCount { get; set; }

        /// <summary>
        /// Override for string representation
        /// </summary>
        /// <returns>string</returns>
        public override string ToString()
        {
            return $"{Country,15}: {CustomerCount}";
        }
    }
}
