using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseAssignment.Models
{
    /// <summary>
    /// Class for mapping query of most poular genres for a customer
    /// </summary>
    class CustomerGenre
    {
        public Customer GenreCustomer { get; set; }
        public List<string> MostPopularGenre { get; set; }

        /// <summary>
        /// Override for string representation
        /// </summary>
        /// <returns>string</returns>
        public override string ToString()
        {
            return $"{GenreCustomer.FirstName} {GenreCustomer.LastName}:\t" + string.Join(", ", MostPopularGenre);
        }

    }
}
