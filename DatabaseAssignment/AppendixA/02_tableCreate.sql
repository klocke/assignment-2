use SuperheroesDb;

-- Creating table for Superhero with autoincrementing primary key
CREATE TABLE Superhero (
	PK_superhero_id INT IDENTITY(1,1) PRIMARY KEY,
	first_name NVARCHAR(100),
	last_name NVARCHAR(100),
	alias NVARCHAR(100) NOT NULL,
	origin NVARCHAR(100),
)

-- Creating table for the Assistant with autoincrementing primary key
CREATE TABLE Assistant (
	PK_assistant_id INT IDENTITY(1,1) PRIMARY KEY,
	first_name NVARCHAR(100) NOT NULL,
	last_name NVARCHAR(100) NOT NULL,
)

-- Creating table for Superpowers with autoincrementing primary key
CREATE TABLE Superpower (
	PK_superpower_id INT IDENTITY(1,1) PRIMARY KEY,
	superpower_name NVARCHAR(100) NOT NULL,
	superpower_description NVARCHAR(255) NOT NULL,
)
