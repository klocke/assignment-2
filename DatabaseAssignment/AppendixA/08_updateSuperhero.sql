use SuperheroesDb;

-- Updating Iron Man in the Superhero table to his more commonly known name using his unique id.
UPDATE Superhero
SET first_name = 'Tony'
WHERE PK_superhero_id = 3;
