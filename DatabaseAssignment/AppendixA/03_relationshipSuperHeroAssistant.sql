use SuperheroesDb;

--Adding column FK_superhero_assistant which is a foreign key that references PK_superhero_id in table Superhero
ALTER TABLE Assistant
ADD FK_superhero_assistant INT NOT NULL
CONSTRAINT FK_superhero_assistant
FOREIGN KEY (FK_superhero_assistant) 
REFERENCES Superhero(PK_superhero_id);
