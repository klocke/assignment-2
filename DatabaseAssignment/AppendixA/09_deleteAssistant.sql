use SuperheroesDb;

-- Deleting poor Jimmy from the Assistant table using both of his names.
DELETE FROM Assistant
WHERE first_name = 'Jimmy' AND last_name = 'Olsen';
