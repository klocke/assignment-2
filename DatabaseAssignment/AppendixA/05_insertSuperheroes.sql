use SuperheroesDb;

-- Inserting some heroes into the Superhero table with first and last names, aliases and origins
INSERT INTO Superhero (first_name, last_name, alias, origin)
VALUES
('Clark', 'Kent', 'Superman', 'Krypton'),
('Diana', 'Prince', 'Wonder Woman', 'Themyscira'),
('Anthony Edward', 'Stark', 'Iron Man', 'New York');
