use SuperheroesDb;

-- Inserting some superpowers and gadgets(?) into the Superpower table. All with name and description
INSERT INTO SuperPower (superpower_name, superpower_description)
VALUES
('Powered Armor', 'A powered armor that gives the user superhuman strength and durability, the ability to fly and an arsenal of weapons'),
('Strength', 'The hero posessess superhuman strength'),
('Flying', 'The hero can fly. No further explanation necessary'),
('Cold Breath', 'The hero has cold breath. I thought this table was for superpowers?!');

-- Adding relationships between heroes and powers
-- Some heroes have many powers and some powers belong to many heroes
INSERT INTO SuperheroPower(superhero_id, superpower_id)
VALUES
(3, 1),
(2, 2),
(2, 3),
(1, 2),
(1, 3),
(1, 4);
