use SuperheroesDb;

-- Creating table SuperheroPower which is a linking table between the Superhero and Superpower tables.
-- Two columns that respectively are foreign keys to each of the original tables, but also part of the composite primary key
CREATE TABLE SuperheroPower(
superhero_id INT NOT NULL,
superpower_id INT NOT NULL,
CONSTRAINT PK_superheroPower PRIMARY KEY (superhero_id, superpower_id),
CONSTRAINT FK_hero_power FOREIGN KEY (superhero_id) REFERENCES Superhero(PK_superhero_id),
CONSTRAINT FK_power_hero FOREIGN KEY (superpower_id) REFERENCES Superpower(PK_superpower_id)
);
