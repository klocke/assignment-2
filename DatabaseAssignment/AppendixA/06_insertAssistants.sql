use SuperheroesDb;

-- Making sure useless heroes have some assistants with first and last names, as well as a number linking them to a Superhero
INSERT INTO Assistant (first_name, last_name, FK_superhero_assistant)
VALUES 
('Virginia', 'Potts', 3),
('Jimmy', 'Olsen', 1),
('Etta', 'Candy', 2);
