﻿using System;
using System.Data.SqlClient;
using DatabaseAssignment.DataAccess;
using DatabaseAssignment.Models;
using System.Collections.Generic;

namespace DatabaseAssignment
{
    class Program
    {
        static void Main(string[] args)
        {

            SqlConnectionStringBuilder builder = new();
            //Console.WriteLine("SQL Server name: ");
            //builder.DataSource = Console.ReadLine();

            builder.DataSource = @"LAPTOP-LJTNCKS5\SQLEXPRESS";

            builder.InitialCatalog = "Chinook";
            builder.IntegratedSecurity = true;

            CustomerRepository customerRepository = new(builder);

            customerRepository.GetCustomers();

            Customer customer = customerRepository.GetCustomerById(1);
            Console.WriteLine(customer);
            Console.WriteLine("-------------------\n");
            customer = customerRepository.GetCustomerByName("Mark", "Taylor");
            Console.WriteLine(customer);
            Console.WriteLine("-------------------\n");
            customerRepository.GetCustomers(10, 12);

            Console.WriteLine("-------------------\n");
            Customer me = new()
            {
                Id = 66,
                FirstName = "Karl",
                LastName = "Lockert",
                PhoneNumber = "12345678",
                Email = "e@mail.com",
                Country = "Norway",
                PostalCode = "9999",
            };

            Console.WriteLine("From console: ");
            Console.WriteLine(me);
            Console.WriteLine("-------------------\n");

            customerRepository.AddCustomer(me);
            me.PhoneNumber = "myPhoneNumber";
            customerRepository.UpdateCustomer(me);



            customer = customerRepository.GetCustomerByName("Karl", "Lockert");
            Console.WriteLine("From Database with (hopefully) updated phone number: ");
            Console.WriteLine(customer);
            Console.WriteLine("-------------------\n");

            customerRepository.GetCustomersByCountryDescending();

            Console.WriteLine("-------------------\n");
            Console.WriteLine("High spenders: ");
            customerRepository.GetHighestSpendersDescending();


            Console.WriteLine("-------------------\n");
            Console.WriteLine("Most popular genre for customer: ");


            for (int id = 1; id < 20; id++)
            {
                Customer c1 = customerRepository.GetCustomerById(id);
                CustomerGenre cg = customerRepository.GetMostPopularGenre(c1);
                Console.WriteLine(cg);
            }



        }

    }
}
